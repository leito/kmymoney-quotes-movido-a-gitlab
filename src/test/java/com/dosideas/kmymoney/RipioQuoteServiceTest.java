package com.dosideas.kmymoney;

import com.dosideas.kmymoney.domain.Quote;
import com.dosideas.kmymoney.service.RipioQuoteService;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class RipioQuoteServiceTest {

    @Autowired
    private RipioQuoteService quoteService;

    @Test
    public void getQuote_hasQuotesForBtc_returnsQuote() throws IOException {
        Quote quote = quoteService.getQuote("BTC");
        assertQuote(quote);
    }

    @Test
    public void getQuote_hasQuotesForEth_returnsQuote() throws IOException {
        Quote quote = quoteService.getQuote("ETH");
        assertQuote(quote);
    }

    @Test
    public void getQuote_hasQuotesForUSDC_returnsQuote() throws IOException {
        Quote quote = quoteService.getQuote("USDC");
        assertQuote(quote);
    }

    public void assertQuote(Quote quote) {
        System.out.println(quote);
        assertNotNull(quote);
        assertNotNull(quote.getDate());
    }

}
