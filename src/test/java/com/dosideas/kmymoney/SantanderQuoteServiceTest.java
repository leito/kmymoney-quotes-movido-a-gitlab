package com.dosideas.kmymoney;

import com.dosideas.kmymoney.domain.Quote;
import com.dosideas.kmymoney.service.SantanderQuoteService;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SantanderQuoteServiceTest {

    @Autowired
    private SantanderQuoteService quoteService;

    @ParameterizedTest
    @ValueSource(strings = {"SUPERGESTION MIX VI CUOTA A", "SUPERFONDO RENTA FIJA DOLARES CUOTA A", "SUPER AHORRO $ CUOTA A"})
    public void getQuote_fund_returnsQuote(String quoteCode) throws IOException {
        Quote quote = quoteService.getQuote(quoteCode);
        assertQuote(quote);
    }
    
    @Test
    public void getQuote_invalid_returnsNull() throws IOException {
        Quote quote = quoteService.getQuote("INVALID_QUOTE");
        assertNull(quote);
    }

    public void assertQuote(Quote quote) {
        System.out.println(quote);
        assertNotNull(quote);
        assertNotNull(quote.getDate());
    }

}
