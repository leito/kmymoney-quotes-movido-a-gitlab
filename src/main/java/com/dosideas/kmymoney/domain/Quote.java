package com.dosideas.kmymoney.domain;

public class Quote {
    private String date;
    private String price;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "{\"date\":\"" + date + "\", \"price\":\"" + price + "\"}";
    }

}
