package com.dosideas.kmymoney.controller;

import com.dosideas.kmymoney.domain.Quote;
import com.dosideas.kmymoney.service.RipioQuoteService;
import com.dosideas.kmymoney.service.SantanderQuoteService;
import java.io.IOException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class QuoteController {

    private final SantanderQuoteService santanderQuoteService;
    private final RipioQuoteService ripioQuoteService;

    public QuoteController(SantanderQuoteService santanderQuoteService, RipioQuoteService ripioQuoteService) {
        this.santanderQuoteService = santanderQuoteService;
        this.ripioQuoteService = ripioQuoteService;
    }

    @GetMapping("/quote/santander/{code}")
    public @ResponseBody String getSantanderQuote(@PathVariable String code) throws IOException {
        try {
            Quote quote = santanderQuoteService.getQuote(code);
            return quote.toString();
        } catch (IOException ex) {
            System.out.println("===============================================");
            System.out.println(ex.getMessage());
            System.out.println("===============================================");
            return null;
        }
    }

    @GetMapping("/quote/ripio/{coin}")
    public @ResponseBody String getRipioQuote(@PathVariable String coin) throws IOException {
        try {
            Quote quote = ripioQuoteService.getQuote(coin);
            return quote.toString();
        } catch (IOException ex) {
            System.out.println("===============================================");
            System.out.println(ex.getMessage());
            System.out.println("===============================================");
            return null;
        }
    }

}
