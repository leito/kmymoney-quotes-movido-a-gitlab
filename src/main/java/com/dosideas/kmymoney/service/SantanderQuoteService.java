package com.dosideas.kmymoney.service;

import com.dosideas.kmymoney.domain.Quote;
import static com.dosideas.kmymoney.util.HttpUtils.getBodyFromUrl;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Service;

@Service
public class SantanderQuoteService {

    private static final String PATTERN_PRICE = "${QUOTECODE}.*?<td align=\"right\" class=\"\">([\\d.,]*)<\\/td>";
    private static final Pattern PATTERN_DATE = Pattern.compile("Rendimientos al (\\d\\d\\/\\d\\d\\/\\d\\d\\d\\d)");

    public Quote getQuote(String code) throws IOException {
        String body = getBodyFromUrl("https://www.santanderrio.com.ar/ConectorPortalStore/Rendimiento", "www.santander.com.ar");
        Quote quote = new Quote();
        code = code.replaceAll("\\$", "\\\\\\$"); //escapar el $ para la regex
        String patternString = PATTERN_PRICE.replace("${QUOTECODE}", code);
        Matcher matcherPrice = Pattern.compile(patternString, Pattern.DOTALL).matcher(body);
        if (matcherPrice.find()) {
            quote.setPrice(matcherPrice.group(1).replace(".", "").replace(",", "."));
        } else {
            System.out.println("===============================================");
            System.out.println("No se encontro el precio. Body a continuacion.");
            System.out.println("===============================================");
            System.out.println(body);
            System.out.println("\n==============================================");
            return null;
        }

        Matcher matcherDate = PATTERN_DATE.matcher(body);
        if (matcherDate.find()) {
            quote.setDate(matcherDate.group(1));
        } else {
            return null;
        }

        return quote;
    }
}
