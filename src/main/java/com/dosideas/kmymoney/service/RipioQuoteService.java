package com.dosideas.kmymoney.service;

import com.dosideas.kmymoney.domain.Quote;
import static com.dosideas.kmymoney.util.HttpUtils.getBodyFromUrl;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Service;

@Service
public class RipioQuoteService {

    private static final String PATTERN_PRICE = "${QUOTECODE}_ARS[\\dA-Za-z_:\",.]*sell_rate\":\"([\\d.]*)\"";

    public Quote getQuote(String coinCode) throws IOException {
        String body = getBodyFromUrl("https://app.ripio.com/api/v3/rates/?country=AR", "www.ripio.com");
        Quote quote = new Quote();
        String patternString = PATTERN_PRICE.replace("${QUOTECODE}", coinCode);
        Matcher matcherPrice = Pattern.compile(patternString, Pattern.DOTALL).matcher(body);
        if (matcherPrice.find()) {
            quote.setPrice(matcherPrice.group(1));
        } else {
            System.out.println("===============================================");
            System.out.println("No se encontro el precio. Body a continuacion.");
            System.out.println("===============================================");
            System.out.println(body);
            System.out.println("\n==============================================");
            return null;
        }

        quote.setDate(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/YYYY")));

        return quote;
    }
}
